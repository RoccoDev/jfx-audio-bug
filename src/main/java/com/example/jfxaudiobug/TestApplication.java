package com.example.jfxaudiobug;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

public class TestApplication extends Application {
    private static String[] args;

    // Prevent GC from freeing player
    private LoopTest test;

    @Override
    public void start(Stage stage) throws IOException {
        switch (args[0]) {
            case "normal" -> test = new LoopNormal();
            case "clip" -> test = new LoopAudioClip();
            case "startstop" -> test = new LoopStartStopTime();
            case "pause" -> test = new LoopPause();
            default -> throw new RuntimeException("invalid test");
        }
        try {
            test.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        TestApplication.args = args;
        launch();
    }
}