package com.example.jfxaudiobug;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public class LoopPause implements LoopTest {
    private MediaPlayer player;

    @Override
    public void run() throws Exception {
        player = new MediaPlayer(new Media(getClass().getResource("/test.mp3").toString()));
        player.setCycleCount(MediaPlayer.INDEFINITE);
        player.setOnEndOfMedia(() -> {
            player.pause();
            player.seek(Duration.ZERO);
            player.play();
        });
        player.play();
    }
}
