package com.example.jfxaudiobug;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public class LoopStartStopTime implements LoopTest {
    private MediaPlayer player;

    @Override
    public void run() throws Exception {
        player = new MediaPlayer(new Media(getClass().getResource("/test.mp3").toString()));
        player.setStartTime(Duration.ZERO);
        player.setStopTime(Duration.seconds(3));
        player.setCycleCount(MediaPlayer.INDEFINITE);
        player.play();
    }
}
