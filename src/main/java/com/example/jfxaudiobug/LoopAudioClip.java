package com.example.jfxaudiobug;

import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class LoopAudioClip implements LoopTest {
    private AudioClip player;

    @Override
    public void run() throws Exception {
        player = new AudioClip(getClass().getResource("/test.mp3").toString());
        player.setCycleCount(MediaPlayer.INDEFINITE);
        player.play();
    }
}
